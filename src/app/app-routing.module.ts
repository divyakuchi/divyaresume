import {  NgModule } from '@angular/core';
import { Routes,RouterModule, Router } from  '@angular/router';
import {HomeComponent} from './home/home.component';
import {ResumeComponent} from './resume/resume.component';


const appRoutes : Routes = [
    {path:'', redirectTo: '/home', pathMatch:'full'},
    {path:'home',component : HomeComponent},
    {path:'resume',component  : ResumeComponent}
];

@NgModule({
    imports:[RouterModule.forRoot(appRoutes)],
    exports:[RouterModule]
})
export class AppRoutingModule {
}