import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   // String interpolation
  servername : string = 'Naga Divya Kuchi Resume';
  allowCheckResume =  false ;
  
  ngOnInit() {
  }

  constructor() {
   }

  getservername(){
    return this.servername;
  }

  onSubmit(form: NgForm){
    console.log(form);
  }

}
